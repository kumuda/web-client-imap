Description:  It's a web application,similar to an email manager like Gmail and ThunderBird, where an user can manage emails from different email accounts. Features such as Search, Send mails , Delete, Import mails from different folders of the mail account are present. GreenMail is used to perform unit testing, Selenium is used for functional testing.    
  
Future version of this project will include combining various accounts of a user and easily switching accounts from one to another,Better user interface than the present one. 

Technologies:  HTML5, jQuery, CSS3, Bootstrap-3, Spring-3.0.5, Maven-2.0, MongoDB-2.4.9, JavaMail API,  JUnit-4.11, Selenium-2.25, GreenMail API, Apache Tomcat-7.0.55,  JavaScript.