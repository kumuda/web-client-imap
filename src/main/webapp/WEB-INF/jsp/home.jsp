<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="css/jquery-te-1.4.0.css">
<link rel="stylesheet" type="text/css" href="css/custom.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap.css.map">
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script type="text/javascript" src="javascript/jquery.js"></script>
<script type="text/javascript" src="javascript/bootstrap.js"></script>
<script type="text/javascript" src="javascript/jquery-te-1.4.0.js"></script>
<script type="text/javascript" src="javascript/home.js"></script>
<script type="text/javascript" src="javascript/jquery.form.js"></script>
<script type="text/javascript" src="javascript/jquery.bootpag.js"></script>
<script type="text/javascript" src="javascript/jquery.autocomplete.js"></script>
<script type="text/javascript" src="javascript/jquery.ui.widget.js"></script>
<script type="text/javascript" src="javascript/jquery.fileupload.js"></script>
<script type="text/javascript" src="js/bootstrap-filestyle.js"></script>
<title>Webclient</title>
</head>
<body>
	<div class="preview__header">
		<div class="form-group row">
			<div class="col-md-2 ">
				<span class="font-20" style="color:indianred;">WebClient</span>
			</div>
			<div class="col-md-6">
				<input type="text" name="Search" placeholder="search" id="search"
							class="form-control focusedInput" style="width: 70%;display:inline"/>
				<button type="button"  id="searchButton"
					class="search-btn search-button" >
					<span class="glyphicon glyphicon-search"></span>
				</button>
			</div>
			<div class="pull-right col-md-2" >
				<span class="pull-right"><a href="logoutServlet"
						style="color: indianred">Logout </a>
				</span>
			</div>
			<div class="pull-right col-md-2">
				<label id="username" class="user-display"><%=session.getAttribute("userName") %></label>
			</div>
		</div>
	</div>
	<div class="body-container">
		<div class="menu">
			<div class="compose-container">
				<input type="button" class="btn btn-compose" name="compose"
					id='compose' value="Compose" />
			</div>
			<div class="compose-container">
				<input type="button" class="btn button-link" id="inbox_link" value="Inbox" />
				<div class="spacer10"></div>
				<input type="button" class="btn button-link" id="sent_link" value="Sent" />
				<div class="spacer10"></div>
				<input type="button" class="btn button-link" id="drafts_link" value="Drafts" />
				<div class="spacer10"></div>
				<input type="button" class="btn button-link" id="trash_link" value="Trash" />
			</div>
		</div>


		<div class="table-container">
			<div align="left" width="100%">
			
			<div class="pull-left" style="padding-right: 2%;padding-bottom: 30px;">
						<select id="mark" class="form-control" >
							<option value="none">None</option>
							<option value="all">All</option>
							<option value="read">Read</option>
							<option value="unRead">Unread</option>
						</select>
					</div>
				<div class="pull-left" style="padding-right: 2%;padding-bottom: 30px;">
					<button type="button" name="Refresh" id="refresh"
						class="btn btn-default" style="color: indianred;">
						<span class="glyphicon glyphicon-refresh"> </span>
					</button>
				</div>
				<div class="pull-left" style="padding-right: 2%;padding-bottom: 30px;">
					<button type="button" name="Delete" id="deleteMails"
						class="btn btn-default"
						style="color: indianred; display: none;">
						<span class="glyphicon glyphicon-trash"> </span>
					</button>
				</div>
			</div>
			<div class="spacer10"></div>
			<table class="table" id="table" border="0">
				<tbody>
				</tbody>
			</table>
			
		
		   <ul class="pagination bootpag">
		   </ul>
	

		</div>
	</div>
	



	<div class="modal fade" id="composeModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">New Message</h4>
				</div>
				<div class="modal-body message-body">
				<form action="sendMail" method="post" id="send_mail_form">
					<label>To</label><input type="text"
						class="form-control focusedInput" id="to_email_ids" name="to" /> <label>Subject</label><input
						type="text" class="form-control focusedInput" id="subject"
						name="subject" />
					<div class="spacer30"></div>
					<textarea class="form-control" id="message_body" style="height: 200px" name="messageBody"></textarea>
					</form>
				</div>
				<div id="attachment_names"></div>
				<div class="modal-footer">
				<div class="pull-left">
				
                    <input type="file" id="attachment" multiple name="attachment">
                  
				</div>
					<button type="button" class="btn btn-primary" id="send_mail">Send</button>
					<button type="button" class="btn btn-default" name="discard draft"
						id="discard_draft" data-dismiss="modal">
						<span class="glyphicon glyphicon-trash"> </span>
					</button>
				</div>
			</div>
		</div>
	</div>



	<div class="modal fade" id="emailModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="email_subject"></h4>
				</div>
				<div class="modal-body" class="container-fluid" id="email_body">
					<div>
						<!-- <div class="pull-left">
							<img src="lib/images/profile_mask2.png" alt="user"
								style="width: 33px; height: 33px">
						</div> -->
						<div class="pull-left" id="div_sender_name">
							<h5 id="sender_name">
								<strong></strong>
							</h5>
						</div>
						<div class="pull-left" id="div_sender_email_id">
							<h5 id="sender_email_id"></h5>
						</div>
						<div class="pull-right" id="div_date_and_time">

							<h5 id="date_and_time"></h5>

						</div>
						<div id="messageBody" class="message-body"></div>

					</div>
					<div id="attachmentArea" style="display: block; clear: both; overflow: auto">
					    <table class="table" id="attachments" border="0">
					    <tbody>
					    
					    </tbody>
					    
					    </table>
					      
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary">Reply</button>
						<button type="button" class="btn btn-primary">Forward</button>
						<button type="button" class="btn btn-default" name="delete-email"
							id="delete_email" data-dismiss="modal">
							<span class="glyphicon glyphicon-trash"> </span>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>


	<div class="modal fade" id="loadModal" tabindex="-1" role="dialog" data-backdrop="false"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content center-div container-fluid">
			  <img alt="images/updating.gif" src="images/updating.gif" />
			</div>
		</div>
	</div>


</body>
</html>