package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import service.ImapService;
import service.MailService;
import service.SyncMailsTaskExecutor;

public class CustomAuthenticationSuccessHandler implements
		AuthenticationSuccessHandler {

	@Autowired
	MailService mailService;
	
	@Autowired
	ImapService imapService;

	@Autowired
	SyncMailsTaskExecutor syncMailsTaskExecutor;

	public void onAuthenticationSuccess(HttpServletRequest httpServletRequest,
			HttpServletResponse httpServletResponse,
			Authentication authentication) throws IOException, ServletException {

		HttpSession session = httpServletRequest.getSession();
		User authUser = (User) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();
		String password = SecurityContextHolder.getContext()
				.getAuthentication().getCredentials().toString();
		model.User user = new model.User();
		user.setUserName(authUser.getUsername());
		user.setPassword(password);
		session.setAttribute("userName", authUser.getUsername());

		try {
			syncMailsTaskExecutor.syncMails(user);
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
			httpServletResponse.sendRedirect("home");
		}
		httpServletResponse.sendRedirect("home");
	}

}
