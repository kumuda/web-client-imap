package controller;

import java.io.IOException;

import javax.mail.Store;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

import model.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import service.ImapService;
import service.UserService;
import service.MailService;
import service.SyncMailsTaskExecutor;
import exceptions.InvalidUsernamePasswordException;

@Controller
public class LoginController {

	@Autowired
	UserService userService;

	@Autowired
	SyncMailsTaskExecutor syncMailsTaskExecutor;

	@Autowired
	MailService mailService;
	
	@Autowired
	ImapService imapService;

	@RequestMapping({ "/", "/login" })
	public String login() {
		System.out.println("inside login method ");
		return "login";
	}

	@RequestMapping(value = "/register")
	public String register() {
		return "register";
	}

	@RequestMapping(value = "/denied")
	public String denied() {
		return "accessdenied";
	}

	@RequestMapping(value = "/addUser", method = RequestMethod.POST)
	public ModelAndView addUser(
			HttpSession session,
			@RequestParam(value = "username", required = false) String userName,
			@RequestParam(value = "password", required = false) String tempPassword)
			throws ServletException, IOException {
		Md5PasswordEncoder passwordEncoder = new Md5PasswordEncoder();
		String password = passwordEncoder.encodePassword(tempPassword, null);

		boolean addStatus = false;
		User user = null;

		int userId = 0;
		try {

			user = new User(userName, password, 1);

		} catch (InvalidUsernamePasswordException e1) {
			return new ModelAndView("register", "message",
					"invalid Username or Password.");
		}

		try {
			userId = userService.addUser(user);
			user = new User();
			user.setUserName(userName);
			user.setPassword(tempPassword);
			session.setAttribute("user", user.getUserName());
			syncMailsTaskExecutor.syncMails(user);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				return new ModelAndView("home");
			}
			return new ModelAndView("home");
		}

		catch (Exception e) {
			e.printStackTrace();
			return new ModelAndView("register", "message",
					"invalid Username or Password.");
		}

	}

	@RequestMapping(value = "/home")
	public String gethome(HttpSession session) {
		return "home";
	}

}
