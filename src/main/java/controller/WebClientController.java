package controller;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TreeSet;

import javax.mail.MessagingException;
import javax.mail.Store;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Attachment;

import org.apache.commons.io.FileUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import service.MailService;
import service.SyncMailsTaskExecutor;
import service.UserService;
import service.WebClientDao;

@Controller
public class WebClientController {

	@Autowired
	MailService mailService;

	@Autowired
	UserService userService;

	@Autowired
	WebClientDao dao;
	
	@Autowired
	SyncMailsTaskExecutor syncMailsTaskExecutor;

	@RequestMapping(value = "/inbox", method = RequestMethod.GET)
	public @ResponseBody
	String getInboxFolder(
			HttpSession session,
			@RequestParam(value = "pageNo", required = false) String pageNumber,
			@RequestParam(value = "search", required = false) String searchString)
			throws IOException {
		int pageNo = 1;
		if (pageNumber != null) {
			pageNo = Integer.parseInt(pageNumber);
		}

		/*String userName = (String) session.getAttribute("userName");
		Store store = (Store) session.getAttribute("store");*/
		model.User user = getUser();
		syncMailsTaskExecutor.syncMails(user);
		HashMap<String, Object> mailsAndPageNo = new HashMap<String, Object>();
		try {
			mailsAndPageNo = mailService.getMails("inbox", pageNo,user.getUserName(), searchString);
			JSONObject mailInfo = new JSONObject();
			mailInfo.put("mails", mailsAndPageNo.get("mails"));
			mailInfo.put("totalPageNos", mailsAndPageNo.get("totalPageNos"));
			return mailInfo.toString();
		} catch (MessagingException e) {

			e.printStackTrace();
			return "";
		}
	}

	@RequestMapping(value = "/showMail", method = RequestMethod.POST)
	public @ResponseBody
	String showMail(
			@RequestParam(value = "messageNumber", required = false) String messageNo,
			@RequestParam(value = "folderName", required = false) String folderName,
			@RequestParam(value = "seen", required = false) String seen,
			HttpSession session) throws Exception {

		int messageNumber = 0;

		if (messageNo != null) {
			messageNumber = Integer.parseInt(messageNo);
		}

		HashMap<String, Object> messageBodyAndAttachments = new HashMap<String, Object>();
		JSONObject jsonObject = new JSONObject();

		/*String userName = (String) session.getAttribute("userName");
		Store store = (Store) session.getAttribute("store");*/
		
		model.User user = getUser();
		try {
			messageBodyAndAttachments = mailService.getMessageBody(
					folderName, messageNumber, seen,user);
			String messageBody = (String) messageBodyAndAttachments
					.get("messageBody");

			ArrayList<Attachment> attachments = (ArrayList<Attachment>) messageBodyAndAttachments
					.get("attachments");

			jsonObject.put("messageBody", messageBody);
			jsonObject.put("attachments", attachments);

		} catch (MessagingException e) {
			jsonObject.put("message", "error");
			e.printStackTrace();
		}
		return jsonObject.toString();
	}

	@RequestMapping(value = "/deleteMails", method = RequestMethod.POST)
	public @ResponseBody
	String deleteMessages(
			@RequestParam(value = "messageNumbers", required = false) List<Integer> messageNumbers,
			@RequestParam(value = "folderName", required = false) String folderName,
			HttpSession session) {

		JSONObject result = new JSONObject();
		/*String userName = (String) session.getAttribute("userName");
		Store store = (Store) session.getAttribute("store");*/
		model.User user = getUser();
		try {
			mailService.deleteMails(folderName, messageNumbers, user);
			result.append("message", "200");
		} catch (MessagingException e) {
			result.append("message", "500");
			e.printStackTrace();

		}
		return result.toString();
	}

	@RequestMapping(value = "/sent", method = RequestMethod.GET)
	public @ResponseBody
	String getSentFolder(
			HttpSession session,
			@RequestParam(value = "pageNo", required = false) String pageNumber,
			@RequestParam(value = "search", required = false) String searchString)
			throws IOException {

		int pageNo = 1;
		if (pageNumber != null) {
			pageNo = Integer.parseInt(pageNumber);
		}
		String userName = (String) session.getAttribute("userName");
		HashMap<String, Object> mailsAndPageNo = null;
		try {
			mailsAndPageNo = mailService.getMails("sent", pageNo,
					userName, searchString);
			JSONObject mailInfo = new JSONObject();
			mailInfo.put("mails", mailsAndPageNo.get("mails"));
			mailInfo.put("totalPageNos", mailsAndPageNo.get("totalPageNos"));
			return mailInfo.toString();
		} catch (MessagingException e) {

			e.printStackTrace();
			return "";
		}
	}

	@RequestMapping(value = "/drafts", method = RequestMethod.GET)
	public @ResponseBody
	String getDraftsFolder(
			HttpSession session,
			@RequestParam(value = "pageNo", required = false) String pageNumber,
			@RequestParam(value = "search", required = false) String searchString)
			throws IOException {
		int pageNo = 1;
		if (pageNumber != null) {
			pageNo = Integer.parseInt(pageNumber);
		}
		String userName = (String) session.getAttribute("userName");
		HashMap<String, Object> mailsAndPageNo = null;
		try {
			mailsAndPageNo = mailService.getMails("drafts", pageNo,
					userName, searchString);
			JSONObject mailInfo = new JSONObject();
			mailInfo.put("mails", mailsAndPageNo.get("mails"));
			mailInfo.put("totalPageNos", mailsAndPageNo.get("totalPageNos"));
			return mailInfo.toString();
		} catch (MessagingException e) {

			e.printStackTrace();
			return "";
		}
	}

	@RequestMapping(value = "/trash", method = RequestMethod.GET)
	public @ResponseBody
	String getTrashFolder(
			HttpSession session,
			@RequestParam(value = "pageNo", required = false) String pageNumber,
			@RequestParam(value = "search", required = false) String searchString)
			throws IOException {
		int pageNo = 1;
		if (pageNumber != null) {
			pageNo = Integer.parseInt(pageNumber);
		}
		HashMap<String, Object> mailsAndPageNo = null;
		String userName = (String) session.getAttribute("userName");
		try {
			mailsAndPageNo = mailService.getMails("trash", pageNo,
					userName, searchString);
			if (mailsAndPageNo != null) {
				JSONObject mailInfo = new JSONObject();
				mailInfo.put("mails", mailsAndPageNo.get("mails"));
				mailInfo.put("totalPageNos", mailsAndPageNo.get("totalPageNos"));
				return mailInfo.toString();
			} else {
				return null;
			}
		} catch (MessagingException e) {

			e.printStackTrace();
			return "";
		}
	}

	@RequestMapping(value = "/sendMail", method = RequestMethod.POST)
	public @ResponseBody
	String sendMail(HttpServletRequest request) throws IOException {
		JSONObject result = new JSONObject();
		StringBuilder buffer = new StringBuilder();
		BufferedReader reader = request.getReader();
		String line;
		while ((line = reader.readLine()) != null) {
			buffer.append(line);
		}
		String email = buffer.toString();
		JSONObject jsonObject = new JSONObject(email);
		model.User user = getUser();
		try {
			mailService.sendMail(jsonObject,user);
			result.append("code", "200");
		} catch (Exception e) {
			result.append("code", "500");
			e.printStackTrace();
		}
		return result.toString();
	}

	@RequestMapping(value = "/uploadAttachment", method = RequestMethod.POST)
	public @ResponseBody
	String uploadAttachment(
			@RequestParam("attachment") MultipartFile uploadedFile,
			HttpSession session) {
		User authUser = (User) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();
		String userName = authUser.getUsername();

		final String UPLOAD_DIR = "uploads";
		final String USERNAME = userName;
		JSONObject jsonObject = new JSONObject();
		try {

			String rootPath = System.getProperty("catalina.home");
			File uploads = new File(rootPath + File.separator + UPLOAD_DIR
					+ File.separator + USERNAME);
			if (!uploads.exists()) {
				uploads.mkdirs();
			}

			byte[] bytes = uploadedFile.getBytes();
			File file = new File(uploads.getAbsolutePath() + File.separator
					+ uploadedFile.getOriginalFilename());
			BufferedOutputStream stream = new BufferedOutputStream(
					new FileOutputStream(file));
			stream.write(bytes);
			stream.close();
			jsonObject.append("result", file.getName());

		} catch (IOException e) {
			jsonObject.append("result", "error");
			e.printStackTrace();
		}

		return jsonObject.toString();

	}

	@RequestMapping(value = "/deleteOutbox", method = RequestMethod.GET)
	public void deleteOutbox() {

		User authUser = (User) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();
		String userName = authUser.getUsername();

		final String UPLOAD_DIR = "uploads";
		final String USERNAME = userName;

		String rootPath = System.getProperty("catalina.home");
		File uploads = new File(rootPath + File.separator + UPLOAD_DIR
				+ File.separator + USERNAME);
		if (uploads.exists()) {
			try {
				FileUtils.cleanDirectory(uploads);
			} catch (IOException e) {

				e.printStackTrace();
			}
		}

	}

	@RequestMapping(value = "/getAddressList", method = RequestMethod.GET)
	public @ResponseBody
	String getAddressList(

	@RequestParam(value = "address", required = false) String addr) {
		TreeSet<String> addresses = dao.getEmailAddress(addr);
		HashMap<String, TreeSet> map = new HashMap<String, TreeSet>();
		map.put("suggestions", addresses);
		JSONObject result = new JSONObject(map);
		return result.toString();
	}

	@RequestMapping(value = "/downloadAttachment", method = RequestMethod.GET)
	public void downloadAttachment(
			// @RequestParam(value = "fileName", required = false) String
			// fileName,
			@RequestParam(value = "folderName", required = false) String folderName,
			@RequestParam(value = "messageNumber", required = false) String messageNo,
			@RequestParam(value = "fileLocation", required = false) String filePath,
			HttpServletResponse response, HttpServletRequest request) {

		int messageNumber = 0;
		if (messageNo != null) {
			messageNumber = Integer.parseInt(messageNo);
		}
		try {
			final int BUFFER_SIZE = 4096;

			ServletContext context = request.getServletContext();
			String appPath = context.getRealPath("");
			File downloadFile = new File(filePath);

			FileInputStream inputStream = new FileInputStream(downloadFile);
			String mimeType = context.getMimeType(filePath);
			if (mimeType == null) {
				mimeType = "application/octet-stream";
			}
			response.setContentType(mimeType);
			response.setContentLength((int) downloadFile.length());
			String headerKey = "Content-Disposition";
			String headerValue = String.format("attachment; filename=\"%s\"",
					downloadFile.getName());
			response.setHeader(headerKey, headerValue);
			OutputStream outStream = response.getOutputStream();
			byte[] buffer = new byte[BUFFER_SIZE];
			int bytesRead = -1;
			while ((bytesRead = inputStream.read(buffer)) != -1) {
				outStream.write(buffer, 0, bytesRead);
			}
			inputStream.close();
			outStream.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@RequestMapping(value = "/logoutServlet")
	public String logout(HttpSession session) {

		Store store = (Store) session.getAttribute("store");
		try {
			store.close();
		} catch (MessagingException e) {
			e.printStackTrace();
		}
		session.invalidate();
		return "login";

	}
	
	private model.User getUser(){
		User authUser = (User) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();
		String password = SecurityContextHolder.getContext()
				.getAuthentication().getCredentials().toString();
		model.User user = new model.User();
		user.setUserName(authUser.getUsername());
		user.setPassword(password);
		
		return user;
	}

}
