package model;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "attachments")
public class Attachment {

	private String userName;
	private String folderName;
	private int messageNumber;
	private String fileName;
	private String fileLocation;
	private int fileSize;
	private int attachmentNumber;

	public Attachment(String userName, String folderName, int messageNumber,
			String fileName, int fileSize, int attachmentNumber) {
		this.userName = userName;
		this.folderName = folderName;
		this.messageNumber = messageNumber;
		this.fileName = fileName;
		this.fileSize = fileSize;
		this.attachmentNumber = attachmentNumber;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getFolderName() {
		return folderName;
	}

	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}

	public int getMessageNumber() {
		return messageNumber;
	}

	public void setMessageNumber(int messageNumber) {
		this.messageNumber = messageNumber;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileLocation() {
		return fileLocation;
	}

	public void setFileLocation(String fileLocation) {
		this.fileLocation = fileLocation;
	}

	public int getFileSize() {
		return fileSize;
	}

	public void setFileSize(int fileSize) {
		this.fileSize = fileSize;
	}

	public int getAttachmentNumber() {
		return attachmentNumber;
	}

	public void setAttachmentNumber(int attachmentNumber) {
		this.attachmentNumber = attachmentNumber;
	}

	@Override
	public int hashCode() {

		int hashcode = this.attachmentNumber * this.fileSize
				* this.messageNumber * this.fileName.hashCode()
				* this.folderName.hashCode() * this.userName.hashCode();
		return hashcode;
	}
}
