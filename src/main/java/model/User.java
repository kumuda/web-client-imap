package model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import exceptions.InvalidUsernamePasswordException;

@Document(collection = "users")
public class User {
	
	public User( String userName,String password,int role) throws InvalidUsernamePasswordException{
		super();
		if (!userName.isEmpty() && userName != null && !password.isEmpty()
				&& password != null) {
			this.userName = userName;
			this.password = password;
			this.role = role;
		} else {
			throw new InvalidUsernamePasswordException(
					"username or password invalid");
		}
	}
	
	public User(){
	
	}

	private String password;
	
	
	@Id
	private String userName;
    private int role;
	
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public int getRole() {
		return role;
	}

	public void setRole(int role) {
		this.role = role;
	}
	
	@Override
	public boolean equals(Object object) {
		boolean result = false;
		if (object == null || object.getClass() != getClass()) {
			result = false;
		} else {
			User user = (User) object;
			if (this.userName == user.getUserName() && this.password == user.getPassword()) {
				result = true;
			}
		}
		return result;
	}

	@Override
	public int hashCode() {
		return (int) this.userName.hashCode()*this.password.hashCode();
	}
	
	@Override
	public String toString() {
		
		String user = "{ \"userName\":" + "\"" + this.getUserName()  + "\"," + "\"password\":" + "\"" + this.getPassword() + "\"}";
		return user;
	}


}
