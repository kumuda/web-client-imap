package model;

import javax.mail.Address;
import javax.mail.internet.InternetAddress;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "addresses")
public class EmailAddress extends InternetAddress {

	private String address;
	private String personal;
	private boolean isGroup;
	private EmailAddress[] group;
	private String userName;

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPersonal() {
		return personal;
	}

	public void setPersonal(String personal) {
		this.personal = personal;
	}

	public boolean isGroup() {
		return isGroup;
	}

	public void setGroup(boolean isGroup) {
		this.isGroup = isGroup;
	}

	public EmailAddress(InternetAddress address, String userName) {

		this.address = address.getAddress();
		this.personal = address.getPersonal();
		this.isGroup = address.isGroup();
		this.userName = userName;

	}

	public EmailAddress(Address[] addresses, String userName) {
		InternetAddress internetAddress;
		if (addresses != null) {
			this.group = new EmailAddress[addresses.length];
			for (int addressCount = 0; addressCount < addresses.length; addressCount++) {

				internetAddress = (InternetAddress) addresses[addressCount];
				this.group[addressCount] = new EmailAddress(internetAddress,
						userName);
			}
		}
	}

	public EmailAddress() {
	}

	public EmailAddress[] getGroup() {
		return group;
	}

	public void setGroup(EmailAddress[] group) {
		this.group = group;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}
