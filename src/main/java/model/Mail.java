package model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import javax.mail.Flags;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage.RecipientType;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "mails")
public class Mail {

	private String userName;
	private EmailAddress from;
	private Date sentDate;
	private Date recievedDate;
	private int messageNumber;
	private EmailAddress to;
	private String subject;
	private String contentType;
	private String messageBody;
	private EmailAddress cc;
	private EmailAddress bcc;
	private String recipients;
	private ArrayList<String> fileNames;
	private String description;
	private String disposition;
	private int lineCount;
	private boolean seen;
	private String folderName;
	private ArrayList<Attachment> attachments;

	public int getMessageNumber() {
		return messageNumber;
	}

	public void setMessageNumber(int messageNumber) {
		this.messageNumber = messageNumber;
	}

	public EmailAddress getTo() {
		return to;
	}

	public void setTo(EmailAddress to) {
		this.to = to;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Mail(Message message, String folderName, String userName)
			throws MessagingException {
		this.userName = userName;
		this.from = new EmailAddress(message.getFrom(), userName);
		this.to = new EmailAddress(message.getRecipients(RecipientType.TO),
				userName);
		this.bcc = new EmailAddress(message.getRecipients(RecipientType.BCC),
				userName);
		this.cc = new EmailAddress(message.getRecipients(RecipientType.CC),
				userName);
		this.subject = message.getSubject();
		this.messageNumber = message.getMessageNumber();
		this.recievedDate = message.getReceivedDate();
		this.sentDate = message.getSentDate();
		this.folderName = folderName;
		this.seen = message.isSet(Flags.Flag.SEEN);
	}

	public Mail() {

	}

	@Override
	public String toString() {

		String message = null;
		message = "{ \"messageNumber\":" + "\"" + this.getMessageNumber()
				+ "\"," + "\"from\":" + "\"" + this.getFrom().getAddress()
				+ "\",\"subject\":" + "\"" + this.getSubject() + "\",\"date\":"
				+ "\"" + this.getRecievedDate() + "\",\"seen\":" + "\""
				+ this.isSeen() + "\",\"folderName\":" + "\""
				+ this.getFolderName() + "\",\"to\":" + "\""
				+ this.getTo().getAddress() + "\"},";

		return message;
	}

	private String getSentDate() {

		return this.sentDate.toString();
	}

	public String getUserName() {
		return userName;
	}

	public void setUsername(String userName) {
		this.userName = userName;
	}

	public EmailAddress getFrom() {
		return from;
	}

	public void setFrom(EmailAddress from) {
		this.from = from;
	}

	public String getRecievedDate() {
		return recievedDate.toString();
	}

	public void setRecievedDate(Date recievedDate) {
		this.recievedDate = recievedDate;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public EmailAddress getCc() {
		return cc;
	}

	public void setCc(EmailAddress cc) {
		this.cc = cc;
	}

	public EmailAddress getBcc() {
		return bcc;
	}

	public void setBcc(EmailAddress bcc) {
		this.bcc = bcc;
	}

	public String getRecipients() {
		return recipients;
	}

	public void setRecipients(String recipients) {
		this.recipients = recipients;
	}

	public ArrayList<String> getFileNames() {
		return fileNames;
	}

	public void setFileNames(ArrayList<String> fileNames) {
		this.fileNames = fileNames;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDisposition() {
		return disposition;
	}

	public void setDisposition(String disposition) {
		this.disposition = disposition;
	}

	public int getLineCount() {
		return lineCount;
	}

	public void setLineCount(int lineCount) {
		this.lineCount = lineCount;
	}

	public boolean isSeen() {
		return seen;
	}

	public void setSeen(boolean seen) {
		this.seen = seen;
	}

	public String getFolderName() {
		return folderName;
	}

	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}

	public String getMessageBody() {
		return messageBody;
	}

	public void setMessageBody(String messageBody) {

		this.messageBody = messageBody;
	}

	public String getMailString() {

		String message = "{ \"messageNumber\":" + "\""
				+ this.getMessageNumber() + "\"," + "\"from\":" + "\""
				+ this.getFrom() + "\",\"subject\":" + "\"" + this.getSubject()
				+ "\",\"date\":" + "\"" + this.getRecievedDate()
				+ "\",\"content\":" + "\"" + this.getMessageBody() + "\"},";

		return message;
	}

	public ArrayList<Attachment> getAttachments() {
		return attachments;
	}

	public void setAttachments(ArrayList<Attachment> attachments) {
		this.attachments = attachments;
	}

}