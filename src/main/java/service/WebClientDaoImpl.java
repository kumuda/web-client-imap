package service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TreeSet;

import model.Attachment;
import model.EmailAddress;
import model.Mail;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.data.domain.Sort;

import util.SpringMongoConfig;

public class WebClientDaoImpl implements WebClientDao {

	@Override
	public ArrayList<Mail> getMails(String folderName, String userName,String searchString,int pageNo) {

		ApplicationContext ctx = new AnnotationConfigApplicationContext(
				SpringMongoConfig.class);
		MongoOperations mongoOperation = (MongoOperations) ctx
				.getBean("mongoTemplate");
		
		Query query = new Query(Criteria.where("userName").is(userName).and("folderName").is(folderName).orOperator(Criteria.where("messageBody").regex(searchString),Criteria.where("subject").regex(searchString))).skip((pageNo-1)*50).with(new Sort(Sort.Direction.DESC,"messageNumber")).limit(50);
		
		List<Mail> mails = mongoOperation.find(query, Mail.class);
		return (ArrayList<Mail>) mails;
		

	}

	@Override
	public void addMail(ArrayList<Mail> mails) {
		ApplicationContext ctx = new AnnotationConfigApplicationContext(
				SpringMongoConfig.class);
		MongoOperations mongoOperation = (MongoOperations) ctx
				.getBean("mongoTemplate");

		List<Mail> listMails = mails;
		mongoOperation.insert(listMails, Mail.class);
	}

	@Override
	public HashMap<String, Object> getMessageBody(String folderName,
			int messageNumber) {

		ApplicationContext ctx = new AnnotationConfigApplicationContext(
				SpringMongoConfig.class);
		MongoOperations mongoOperation = (MongoOperations) ctx
				.getBean("mongoTemplate");

		User authUser = (User) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();
		String userName = authUser.getUsername();

		Mail mail = mongoOperation.findOne(
				new Query(Criteria.where("messageNumber").is(messageNumber)
						.and("userName").is(userName).and("folderName")
						.is(folderName)), Mail.class);

		HashMap<String, Object> messageContent = new HashMap<String, Object>();
		if (mail.getMessageBody() != null) {
			messageContent.put("messageBody", mail.getMessageBody());
		}
		if (mail.getFileNames() != null && mail.getFileNames().size() != 0) {
			messageContent.put("fileNames", mail.getFileNames());
		}

		return messageContent;
	}

	@Override
	public void setMessageBody(String folderName, int messageNumber,
			HashMap<String, Object> messageContent, String seen) {

		ApplicationContext ctx = new AnnotationConfigApplicationContext(
				SpringMongoConfig.class);
		MongoOperations mongoOperation = (MongoOperations) ctx
				.getBean("mongoTemplate");

		User authUser = (User) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();
		String userName = authUser.getUsername();
		ArrayList<String> fileNames = (ArrayList<String>) messageContent
				.get("fileNames");

		Query query = new Query(Criteria.where("messageNumber")
				.is(messageNumber).and("userName").is(userName)
				.and("folderName").is(folderName));

		Update update = new Update();
		update.set("messageBody", (String) messageContent.get("messageBody"));
		update.set("seen", true);
		if (messageContent.get("fileNames") != null) {
			update.set("fileNames",
					(ArrayList<String>) messageContent.get("fileNames"));
		}

		if (messageContent.get("attachments") != null) {
			update.set("attachments",
					(ArrayList<Attachment>) messageContent.get("attachments"));
		}

		mongoOperation.updateFirst(query, update, Mail.class);
	}

	@Override
	public void deleteMails(String folderName, List<Integer> messageNumbers) {

		System.out.println("inside delete mails webcliend dao impl");
		ApplicationContext ctx = new AnnotationConfigApplicationContext(
				SpringMongoConfig.class);
		MongoOperations mongoOperation = (MongoOperations) ctx
				.getBean("mongoTemplate");

		User authUser = (User) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();
		String userName = authUser.getUsername();
		Update update = null;
		for (Integer messageNo : messageNumbers) {
			int messageNumber = messageNo;
			/*update = new Update();
			update.set("folderName", "trash");*/
			/*mongoOperation.updateFirst(new Query(Criteria
					.where("messageNumber").is(messageNumber).and("userName")
					.is(userName).and("folderName").is(folderName)), update,
					Mail.class);*/
			mongoOperation.remove(new Query(Criteria
					.where("messageNumber").is(messageNumber).and("userName")
					.is(userName).and("folderName").is(folderName)),Mail.class);
		}
	}

	public TreeSet<String> getEmailAddress(String addr) {

		ApplicationContext ctx = new AnnotationConfigApplicationContext(
				SpringMongoConfig.class);
		MongoOperations mongoOperation = (MongoOperations) ctx
				.getBean("mongoTemplate");

		System.out.println("Address===" + addr);
		User authUser = (User) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();
		String userName = authUser.getUsername();

		Query query = new Query(Criteria.where("userName").is(userName).and("address").regex(addr));

		List<EmailAddress> addresses = mongoOperation.find(query,
				EmailAddress.class);

		TreeSet<String> emailAddresses = new TreeSet<String>();
		for (EmailAddress address : addresses) {
			emailAddresses.add(address.getAddress());
		}

		return emailAddresses;
	}

	@Override
	public void saveAddresses(ArrayList<EmailAddress> addresses) {
		ApplicationContext ctx = new AnnotationConfigApplicationContext(
				SpringMongoConfig.class);
		MongoOperations mongoOperation = (MongoOperations) ctx
				.getBean("mongoTemplate");

		List<EmailAddress> emailAddresses = addresses;
		mongoOperation.insert(emailAddresses, EmailAddress.class);
		
	}

	@Override
	public String getPersonal(String userName,String addr) {
		ApplicationContext ctx = new AnnotationConfigApplicationContext(
				SpringMongoConfig.class);
		MongoOperations mongoOperation = (MongoOperations) ctx
				.getBean("mongoTemplate");

		String userPersonal = null;
		EmailAddress address = mongoOperation.findOne(
				new Query(Criteria.where("userName").is(userName).and("address").is(addr)),
				EmailAddress.class);

		if (address != null) {
			userPersonal = address.getPersonal();
		}

		return userPersonal;
	}

	@Override
	public Attachment getFilesLocations(String fileName, int messageNumber,
			String folderName) {
		User authUser = (User) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();
		String userName = authUser.getUsername();

		ApplicationContext ctx = new AnnotationConfigApplicationContext(
				SpringMongoConfig.class);
		MongoOperations mongoOperation = (MongoOperations) ctx
				.getBean("mongoTemplate");

		Mail mail = mongoOperation.findOne(
				new Query(Criteria.where("userName").is(userName)
						.and("folderName").is(folderName).and("messageNumber")
						.is(messageNumber)), Mail.class);

		ArrayList<Attachment> attachments = mail.getAttachments();
		Attachment attachment = null;
		if (attachments.size() != 0) {
			for (Attachment eachAttachment : attachments) {
				if (eachAttachment.getFileName() == fileName) {
					attachment = eachAttachment;
				}
			}
		}
		return attachment;

	}

	@Override
	public void setFileLocation(String absolutePath, int messageNumber,
			String folderName, String fileName) {
		User authUser = (User) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();
		String userName = authUser.getUsername();

		ApplicationContext ctx = new AnnotationConfigApplicationContext(
				SpringMongoConfig.class);
		MongoOperations mongoOperation = (MongoOperations) ctx
				.getBean("mongoTemplate");

		Query query = new Query(Criteria.where("messageNumber")
				.is(messageNumber).and("userName").is(userName)
				.and("folderName").is(folderName).and("attachments.fileName")
				.is(fileName));

		Update update = new Update().push("attachments.$.fileLocation",
				absolutePath);

		mongoOperation.updateFirst(query, update, Mail.class);
	}

	@Override
	public int getLatestMessageNumber(String folderName,int pageNo) {
		
		User authUser = (User) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();
		String userName = authUser.getUsername();
		
		ApplicationContext ctx = new AnnotationConfigApplicationContext(
				SpringMongoConfig.class);
		MongoOperations mongoOperation = (MongoOperations) ctx
				.getBean("mongoTemplate");
		int messageNumber = 0;
		Query query = 
				new Query(Criteria.where("userName").is(userName).and("folderName")
						.is(folderName)).skip(0).with(new Sort(Sort.Direction.DESC,"messageNumber")).limit(1);
		
				
				Mail mail = mongoOperation.findOne(query,Mail.class);
				if(mail != null){
				messageNumber = mail.getMessageNumber();
				}
				
				return messageNumber;
	}

	@Override
	public long getMailCount(String searchString,String folderName,String userName){
		ApplicationContext ctx = new AnnotationConfigApplicationContext(
				SpringMongoConfig.class);
		MongoOperations mongoOperation = (MongoOperations) ctx
				.getBean("mongoTemplate");
		Query query = new Query(Criteria.where("userName").is(userName).and("folderName").is(folderName).orOperator(Criteria.where("messageBody").regex(searchString),Criteria.where("subject").regex(searchString)));
		
		long mailCount = 0;
		mailCount = mongoOperation.count(query, Mail.class);
		
		return mailCount;
	}

	@Override
	public int getLatestMessageNumber(String folderName, String userName) {
		
		ApplicationContext ctx = new AnnotationConfigApplicationContext(
				SpringMongoConfig.class);
		MongoOperations mongoOperation = (MongoOperations) ctx
				.getBean("mongoTemplate");
		int messageNumber = 0;
		Query query = 
				new Query(Criteria.where("userName").is(userName).and("folderName")
						.is(folderName)).skip(0).with(new Sort(Sort.Direction.DESC,"messageNumber")).limit(1);
		
				
				Mail mail = mongoOperation.findOne(query,Mail.class);
				if(mail != null){
				messageNumber = mail.getMessageNumber();
				}
				
				return messageNumber;
		
	}

}
