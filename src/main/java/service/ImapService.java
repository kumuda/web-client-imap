package service;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Store;

import model.User;

import org.json.JSONObject;

public interface ImapService {
	public int getMessageCount(String folderName,User user) throws MessagingException;
	
	public Message[] getMessages(String folderName, int to, int from,User user)throws MessagingException;
	
	public HashMap<String,Object> getMessageBody(String folderName,int messageNumber,String seen,User user)throws Exception;
	
	public void deleteMails(String folderName, List<Integer> messageNumbers,User user)throws MessagingException;
	
	public String sendMail(JSONObject jsonObject,User user)throws IOException,MessagingException;
	
	/*public Store getStore(User user)throws MessagingException;*/
	
	public HashMap<String,Store> getStoreMap();
	
}
