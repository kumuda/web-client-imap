package service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TreeSet;

import javax.mail.MessagingException;
import javax.mail.Store;

import model.Mail;
import model.User;

import org.json.JSONObject;

public interface MailService {

	public HashMap<String, Object> getMails(String folderName, int pageNo,
			String userName, String searchString) throws MessagingException,
			IOException;

	public HashMap<String, Object> getMessageBody(String folderName,
			int messageNumber, String seen,User user)
			throws Exception;

	public void deleteMails(String folderName, List<Integer> messageNumbers,User user) throws MessagingException;

	public String sendMail(JSONObject jsonObject,User user) throws IOException,
			MessagingException;


	public ArrayList<Mail> getMailsFromImap(String folderName, int to,
			int from, User user) throws MessagingException;

	/*public Store getStore(User user) throws MessagingException;

	public int getMessageCountFromImap(String folderName, Store store)
			throws MessagingException;
			
	public TreeSet<String> getEmailAddress(String addr);

	public int getLatestMessageNumberFromDb(String folderName, String userName);*/

}