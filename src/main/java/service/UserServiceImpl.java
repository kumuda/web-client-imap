package service;

import java.util.concurrent.atomic.AtomicInteger;

import model.User;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import util.SpringMongoConfig;

public class UserServiceImpl implements UserService {
	
   private static final	AtomicInteger counter = new AtomicInteger();

	public User loadUserByUsername(String userName) {

		ApplicationContext ctx = new AnnotationConfigApplicationContext(
				SpringMongoConfig.class);
		MongoOperations mongoOperation = (MongoOperations) ctx.getBean("mongoTemplate");
		 User user = mongoOperation.findOne(
				 new Query(Criteria.where("userName").is(userName)),
				 User.class);
		System.out.println(user.toString());
		return user;

	}

	public int addUser(User user) {
		ApplicationContext ctx = new AnnotationConfigApplicationContext(
				SpringMongoConfig.class);
		MongoOperations mongoOperation = (MongoOperations) ctx
				.getBean("mongoTemplate");
		mongoOperation.save(user);

		return 0;
	}

	
}
