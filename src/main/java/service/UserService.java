package service;

import model.User;

public interface UserService {

	public User loadUserByUsername(String userName);

	public int addUser(User user);

	
}
