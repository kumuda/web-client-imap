package service;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.FetchProfile;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import model.Attachment;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.json.JSONObject;

import com.icegreen.greenmail.util.ServerSetupTest;
import com.sun.mail.imap.IMAPFolder;
import com.sun.mail.smtp.SMTPTransport;

import model.User;

public class ImapServiceImpl implements ImapService {
	
	private static HashMap<String,Store> storeMap = new HashMap<String,Store>();
	
	private static HashMap<String,Session> sessionMap = new HashMap<String,Session>();
	
	private static ImapServiceImpl imapService;

	private Folder getFolder(String folderName,User user) throws MessagingException{
		Store store = getStore(user);
		Folder[] folders = store.getDefaultFolder().list("*");
		Folder returnFolder = null;
		for (Folder folder : folders) {
			if ((folder.getName().toLowerCase()).contains(folderName
					.toLowerCase())) {
				returnFolder = folder;
				break;
			}
		}
	
		return returnFolder;
	}

	@Override
	public int getMessageCount(String folderName,User user) throws MessagingException {
		Folder folder = getFolder(folderName,user);
		folder.open(Folder.READ_ONLY);
		int messageCount = folder.getMessageCount();
		folder.close(false);
		return messageCount;
	}

	@Override
	public Message[] getMessages(String folderName, int from,int to,User user) throws MessagingException {
		Folder folder = getFolder(folderName,user);
		folder.open(Folder.READ_ONLY);
		Message[] messages = folder.getMessages(to,from);
		FetchProfile fetchProfile = new FetchProfile();
		fetchProfile.add(FetchProfile.Item.ENVELOPE);
		fetchProfile.add(FetchProfile.Item.FLAGS);
		folder.fetch(messages, fetchProfile);
		folder.close(false);
		return messages;
	}
	@Override
	public HashMap<String,Object> getMessageBody(String folderName,int messageNumber,String seen,User user) throws Exception{
	
		Folder folder = getFolder(folderName,user);
		folder.open(Folder.READ_WRITE);
		Message message = folder.getMessage(messageNumber);
		if(seen.equals("false")){
			message.setFlag(Flags.Flag.SEEN, true);
		}
		Object content = message.getContent();
		String messageBody = null;
		HashMap<String,Object> mailContent = new HashMap<String,Object>();
		String fileName = null;
		String fileLocation = null;
		Attachment attachment = null ;
		ArrayList<Attachment> attachments = new ArrayList<Attachment>();

		String rootPath = System.getProperty("catalina.home");
		final String ATTACHMENT_DIR = "attachments";
		File attachmentDirectory = new File(rootPath + File.separator +ATTACHMENT_DIR+File.separator+folderName);
		if (!attachmentDirectory.exists()) {
			attachmentDirectory.mkdirs();
		}
		
		if(content instanceof String) 
		{
		  messageBody = (String) content;	
		}
		else if (content instanceof Multipart) {
			Multipart multipart = (Multipart) content;
			for (int index = 0; index < multipart.getCount(); index++) {
				Object bodyPart = multipart.getBodyPart(index).getContent();
				if(bodyPart instanceof String){
					messageBody = (String) multipart.getBodyPart(index).getContent() ;
				}
				else if(bodyPart instanceof InputStream){
					fileName = multipart.getBodyPart(index).getFileName();
					attachment = new Attachment(user.getUserName(), folderName, messageNumber, fileName,multipart.getBodyPart(index).getSize(),index);
					String hashCode =Integer.toString(attachment.hashCode());
					String fileExtension = FilenameUtils.getExtension(fileName);
					String uniqueFileName = hashCode.concat("."+fileExtension);
					fileLocation = attachmentDirectory.getAbsolutePath()+File.separator+uniqueFileName;
					attachment.setFileLocation(fileLocation);
					getAttachment(attachment,user);
					attachments.add(attachment);		
				}
			}
		}
		mailContent.put("messageBody", messageBody);
		mailContent.put("attachments", attachments);
		folder.close(true);
		return mailContent;
		
	}

	@Override
	public void deleteMails(String folderName, List<Integer> messageNumbers,User user) throws MessagingException {
		
		Folder folder = getFolder(folderName,user);
		folder.open(Folder.READ_WRITE);
		System.out.println("inside delete mails imap service impl");
		if(messageNumbers != null && messageNumbers.size()!=0){
		for(Integer messageNumber : messageNumbers){
			int messageNo = messageNumber;
			System.out.println(messageNo);
			Message message = folder.getMessage(messageNo);
			if(message != null){
			message.setFlag(Flags.Flag.DELETED, true);
			}
		}
		folder.close(true);
	  }
		
	}
	
	private Session getSession(User user) {
		final String userName = user.getUserName();
		final String password = user.getPassword();
		Session session;
		if(sessionMap.containsKey(userName)){
			session = sessionMap.get(userName);
		}
		else{
		Properties properties = System.getProperties();
		if (userName.contains("gmail")) {
			properties.setProperty("mail.smtp.host", "smtp.gmail.com");
			properties.put("mail.smtp.auth", "true");
			properties.put("mail.smtp.starttls.enable", "true");
			properties.put("mail.smtp.port", "25");
		}

		else if (userName.contains("yahoo")) {
			properties.setProperty("mail.smtp.host", "smtp.mail.yahoo.com");
			properties.put("mail.smtp.auth", "true");
			properties.put("mail.smtp.starttls.enable", "true");
			properties.put("mail.smtp.port", "587");
		}
		
		session = Session.getInstance(properties,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(userName, password);
					}
				});
		}
		return session;
	}
	
	private File[] getAttachmentsFromFileSystem(String userName) throws IOException {

		final String UPLOAD_DIR = "uploads";
		String rootPath = System.getProperty("catalina.home");
		 
		 File[] files = null;
		 
		File fileUploads = new File(rootPath + File.separator + UPLOAD_DIR + File.separator + userName);
		if (fileUploads.exists() && fileUploads.listFiles().length != 0) {
			
			files = fileUploads.listFiles();
		}
		
		return files;
	}

	@Override
	public String sendMail(JSONObject jsonObject,User user) throws IOException, MessagingException {
		Session session = getSession(user);

		String subject = (String) jsonObject.get("subject");
		String messageBody = (String) jsonObject.get("snippet");
		InternetAddress[] addresses = (InternetAddress[]) jsonObject.get("addresses");
		File[] attachments = getAttachmentsFromFileSystem(user.getUserName());
		Message newMessage = new MimeMessage(session);
		newMessage.setFrom(new InternetAddress(user.getUserName()));
		newMessage.setRecipients(Message.RecipientType.TO,addresses);
		newMessage.setSubject(subject);
		BodyPart messageBodyPart = null;
		BodyPart messageText = new MimeBodyPart();
		messageText.setContent(messageBody, "text/html");
		Multipart multipart = new MimeMultipart("mixed");
		multipart.addBodyPart(messageText);
		DataSource source = null;
		if(attachments != null){
		for (File attachment : attachments) {
			source = new FileDataSource(attachment.getAbsolutePath());
			messageBodyPart = new MimeBodyPart();
			messageBodyPart.setDataHandler(new DataHandler(source));
			messageBodyPart.setFileName(attachment.getName());
			multipart.addBodyPart(messageBodyPart);
		}
	  }
		newMessage.setContent(multipart);
		if(user.getUserName().contains("localhost")){
			SMTPTransport smtpTransport = (SMTPTransport) session.getTransport("imap");
	        smtpTransport.connect("127.0.0.1",user.getUserName(), user.getPassword());
	        smtpTransport.sendMessage(newMessage, newMessage.getAllRecipients());
	        System.out.println("message sent");
		}
		else {
		Transport.send(newMessage);
		}

		return null;
	}
	
	private void getAttachment(Attachment attachment,User user) throws Exception{
		
		Folder folder = getFolder(attachment.getFolderName(),user);
		folder.open(Folder.READ_ONLY);
		Message message = folder.getMessage(attachment.getMessageNumber());
		
		Multipart multipart = (Multipart) message.getContent();
		
		InputStream inputStream = multipart.getBodyPart(attachment.getAttachmentNumber()).getInputStream();
		byte[] bytes = IOUtils.toByteArray(inputStream);
		
		File file = new File(attachment.getFileLocation());
		BufferedOutputStream stream = new BufferedOutputStream(
		new FileOutputStream(file));
		stream.write(bytes);
		stream.close();
		folder.close(false);
		
	}

	private Store getStore(model.User user) throws MessagingException {
		Store store;
		if (storeMap.containsKey(user.getUserName())) {
			store = storeMap.get(user.getUserName());
			if(!store.isConnected()){
				store = createStore(user);
			}
		}
		else {
			store = createStore(user);
		}
		return store;
	}

	private Store createStore(model.User user) throws MessagingException{
		Properties props = System.getProperties();
		props.setProperty("mail.store.protocol", "imaps");
		Session session = Session.getDefaultInstance(props, null);
		Store store = session.getStore("imaps");
		if (user.getUserName().contains("gmail")) {
			store.connect("imap.gmail.com", user.getUserName(),
					user.getPassword());
		} else if (user.getUserName().contains("yahoo")) {
			store.connect("imap.mail.yahoo.com", 993, user.getUserName(),
					user.getPassword());
		}
		storeMap.put(user.getUserName(), store);
		System.out.println("Store object--->" + store.toString());
		return store;
	}
	
	public static ImapServiceImpl getInstance() {
		if (imapService == null) {
			imapService = new ImapServiceImpl();
		}
		return imapService;
	}
	
	@Override
	public HashMap<String,Store> getStoreMap(){
		return storeMap;
	}

	public static HashMap<String,Session> getSessionMap() {
		return sessionMap;
	}

	public static void setSessionMap(HashMap<String,Session> sessionMap) {
		ImapServiceImpl.sessionMap = sessionMap;
	}
}
