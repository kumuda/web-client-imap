package service;

import javax.mail.MessagingException;
import javax.mail.Store;

import model.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class SyncMailsTaskExecutor {
	
	@Autowired
	MailService mailService;

	@Autowired
	ImapService imapService;
	
	@Autowired
	WebClientDao dao;
	
	public class SyncMails implements Runnable {

		private User user;

		
		public SyncMails(User user) {
			this.user = user;
			
		}

		@Override
		public void run() {
			try {
				System.out.println("inside sync mails run method");
				System.out.println("web client service in run method---->" +  mailService);
				String[] folderNames = { "inbox", "sent", "trash", "drafts" };
				for (String folderName : folderNames) {
					int messageCount = imapService.getMessageCount(folderName, user);
					int dbLatestMessageNumber = dao.getLatestMessageNumber(folderName, user.getUserName());
					System.out.println("MEssage count---->" + messageCount);
					System.out.println("Latest message count dao--->"
							+ dbLatestMessageNumber);
					if (messageCount > dbLatestMessageNumber) {
						mailService.getMailsFromImap(folderName,
								messageCount, dbLatestMessageNumber + 1,user);
					}

				}
			} catch (MessagingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		public User getUserName() {
			return user;
		}

		public void setUser(User user) {
			this.user = user;
		}


	}
	
	
	private TaskExecutor taskExecutor;

	  public SyncMailsTaskExecutor(TaskExecutor taskExecutor) {
	    this.taskExecutor = taskExecutor;
	  }
	  
	  public void syncMails(User user){
		  System.out.println("web client service object---->" + mailService);
		  this.taskExecutor.execute(new SyncMails(user));
	  }
}
