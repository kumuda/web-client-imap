package service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TreeSet;

import model.Attachment;
import model.EmailAddress;
import model.Mail;

public interface WebClientDao {

	public ArrayList<Mail> getMails(String folderName, String userName,
			String searchString, int pageNo);

	public void addMail(ArrayList<Mail> mails);

	public HashMap<String, Object> getMessageBody(String folderName,
			int messageNumber);

	public void setMessageBody(String folderName, int messageNumber,
			HashMap<String, Object> messageContent, String seen);

	public void deleteMails(String folderName, List<Integer> messageNumbers);

	public TreeSet<String> getEmailAddress(String addr);

	public void saveAddresses(ArrayList<EmailAddress> addresses);

	public String getPersonal(String userName,String addr);

	public Attachment getFilesLocations(String fileName, int messageNumber,
			String folderName);

	public void setFileLocation(String absolutePath, int messageNumber,
			String folderName, String fileName);

	public int getLatestMessageNumber(String folderName, int pageNo);

	public int getLatestMessageNumber(String folderName, String userName);

	public long getMailCount(String searchString, String folderName,
			String userName);

}
