package service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;

import model.EmailAddress;
import model.Mail;
import model.User;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

public class MailServiceImpl implements MailService{

	@Autowired
	private ImapService imapService;
	
	@Autowired
	private WebClientDao dao;
	
	@Override
	public HashMap<String, Object> getMails(String folderName, int pageNo,String userName,String searchString) throws MessagingException, IOException {
		long messageCount = dao.getMailCount(searchString, folderName,userName);
		HashMap<String, Object> mailsAndPageNos = new HashMap<String, Object>();
	    ArrayList<Mail> mails = dao.getMails(folderName, userName, searchString, pageNo);
		mailsAndPageNos.put("mails", mails);
		mailsAndPageNos.put("totalPageNos", Math.ceil((messageCount / 50)));
		return mailsAndPageNos;
	}
	
	@Override
	public HashMap<String, Object> getMessageBody(String folderName,
		int messageNumber,String seen,User user) throws Exception {
		HashMap<String,Object> messageContent = dao.getMessageBody(folderName,messageNumber);
		if(messageContent.isEmpty() ){
			messageContent = imapService.getMessageBody(folderName, messageNumber,seen,user);
			dao.setMessageBody(folderName, messageNumber,messageContent,seen);
		}
		return messageContent;
	}

	@Override
	public void deleteMails(String folderName, List<Integer> messageNumbers,User user) throws MessagingException {
		imapService.deleteMails(folderName,messageNumbers,user);
		dao.deleteMails(folderName,messageNumbers);
	}

	@Override
	public String sendMail(JSONObject jsonObject,User user) throws IOException, MessagingException {
		String toString = (String) jsonObject.get("to");
		String[] toArray = toString.split(",");
		InternetAddress[] addresses = new InternetAddress[toArray.length];
		InternetAddress address ;
		String personal = null;
		for(int count=0;count<toArray.length;count++){
			personal = dao.getPersonal(user.getUserName(),toArray[count]);
			if(personal != null){
			address = new InternetAddress(toArray[count], personal);
			}
			else{
				address = new InternetAddress(toArray[count]);
			}
			addresses[count] = address;
		}
		jsonObject.put("addresses", addresses);
		personal = dao.getPersonal(user.getUserName(),user.getUserName());
		jsonObject.put("personal", personal);
		imapService.sendMail(jsonObject,user);
		
		return null;
	}

	/*@Override
	public TreeSet<String> getEmailAddress(String addr) {
		return dao.getEmailAddress(addr);
	}

	private ArrayList<Mail> getMailsFromDao(String folderName,String userName,String searchString, int pageNo){
		
		ArrayList<Mail> mails =  dao.getMails(folderName,userName,searchString,pageNo);		
		return mails;
	}
	*/
	
	private ArrayList<EmailAddress> getAddresses(Message message, String userName) throws MessagingException {
		InternetAddress[] address;
		EmailAddress emailAddress;
		ArrayList<EmailAddress> addresses = new ArrayList<EmailAddress>();
			if (message.getFrom() != null) {
				for (InternetAddress addr : (InternetAddress[])message.getFrom()) {
					emailAddress = new EmailAddress(addr,userName);
					addresses.add(emailAddress);
				}
			}
			if (message.getRecipients(RecipientType.TO) != null) {
				address = (InternetAddress[]) message
						.getRecipients(RecipientType.TO);
				for (InternetAddress addr : address) {
					emailAddress = new EmailAddress(addr,userName);
					addresses.add(emailAddress);
				}
			}
			if (message.getRecipients(RecipientType.CC) != null) {
				address = (InternetAddress[]) message
						.getRecipients(RecipientType.CC);
				for (InternetAddress addr : address) {
					emailAddress = new EmailAddress(addr,userName);
					addresses.add(emailAddress);
				}
			}
			if (message.getRecipients(RecipientType.BCC) != null) {
				address = (InternetAddress[]) message
						.getRecipients(RecipientType.BCC);
				for (InternetAddress addr : address) {
					emailAddress = new EmailAddress(addr,userName);
					addresses.add(emailAddress);
				}
			}
		return addresses;
	}

	@Override
	public ArrayList<Mail> getMailsFromImap(String folderName,int to,int from,User user) throws MessagingException{
		Mail mail;
		ArrayList<Mail> mails = new ArrayList<Mail>();
		ArrayList<EmailAddress> addresses = new ArrayList<EmailAddress>();
		Message[] messages = imapService.getMessages(folderName,to,from,user);
		Arrays.sort(messages, new Comparator<Message>() {
			@Override
			public int compare(Message message1, Message message2) {
				return ((-message1.getMessageNumber()) - (-message2
						.getMessageNumber()));
			}
		});
		for (int index = 0 ; index<messages.length;index++) {
			mail = new Mail(messages[index], folderName,user.getUserName());
			addresses.addAll(getAddresses(messages[index],user.getUserName()));
			mails.add(mail);
		}
		dao.addMail(mails);
		dao.saveAddresses(addresses);
		return mails;
	}
	
}