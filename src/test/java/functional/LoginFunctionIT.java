package functional;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import javax.mail.MessagingException;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class LoginFunctionIT extends FunctionIT {
	
	/*@Before
	public void setup() throws IOException, MessagingException{
		this.addMessages();
	}*/
	
	
	private void emptyUsername(WebDriver driver, WebElement submit) {
		submit.click();
		WebElement cssMessage = driver.findElement(By
				.cssSelector("input:invalid"));
		String inputId = cssMessage.getAttribute("id");
		assertEquals("username", inputId);

	}

	private void emptyPassword(WebDriver driver, WebElement submit) {
		WebElement username = driver.findElement(By
				.xpath("//*[@id=\"username\"]"));
		username.clear();
		username.sendKeys("abcd");
		submit.click();
		WebElement cssMessage = driver.findElement(By
				.cssSelector("input:invalid"));
		String inputId = cssMessage.getAttribute("id");
		assertEquals("loginPassword", inputId);
	}

	private void incorrectLogin(WebDriver driver, WebElement submit) {
		WebElement username = driver.findElement(By
				.xpath("//*[@id=\"username\"]"));
		username.clear();
		username.sendKeys("abcd");
		WebElement password = driver.findElement(By
				.xpath("//*[@id=\"loginPassword\"]"));
		password.sendKeys("abcd");
		submit.click();
		WebElement errorMessage = driver.findElement(By
				.xpath("/html/body/div[2]/div/div/form/div"));
		assertEquals("User Name or Password incorrect.", errorMessage.getText());
	}

	private void correctLogin(WebDriver driver) throws InterruptedException {
		WebElement username = driver.findElement(By
				.xpath("//*[@id=\"username\"]"));
		username.clear();
		username.sendKeys("incredible.kumda@gmail.com");
		WebElement password = driver.findElement(By
				.xpath("//*[@id=\"loginPassword\"]"));
		password.clear();
		password.sendKeys("MADHURIMULGUND");
		WebElement submit = driver.findElement(By
				.xpath("/html/body/div[2]/div/div/form/fieldset[3]/button"));
		submit.submit();
		this.getWait().until(
				ExpectedConditions.presenceOfElementLocated(By
						.xpath("//*[@id=\"table\"]")));
		Thread.sleep(2000);
		WebElement userName = driver.findElement(By
				.xpath("//*[@id=\"username\"]"));
		assertEquals(
				"incredible.kumda@gmail.com",
				userName.getText());

	}

	@Test
	public void test() throws Exception {
		this.getWebBrowser();
		WebDriver driver = this.getDriver();
		WebElement submit = driver.findElement(By
				.xpath("/html/body/div[2]/div/div/form/fieldset[3]/button"));
		this.emptyUsername(driver, submit);
		this.emptyPassword(driver, submit);
		this.incorrectLogin(driver, submit);
		this.correctLogin(driver);
		this.quitWebBrowser();

	}

}
