package functional;

import static org.junit.Assert.assertEquals;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

public class HomePageIT extends FunctionIT {

	public void composeButtonTest(WebDriver driver) throws InterruptedException {
		Thread.sleep(2000);
		WebElement composeButton = driver.findElement(By
				.xpath("//*[@id=\"compose\"]"));
		composeButton.click();
		Thread.sleep(2000);
		Set windows = driver.getWindowHandles();
		Iterator iterator = windows.iterator();
		while (iterator.hasNext()) {
			String popupHandle = iterator.next().toString();
			driver.switchTo().window(popupHandle);
			WebElement modalTitle = driver.findElement(By
					.xpath("//*[@id=\"myModalLabel\"]"));
			assertEquals("New Message", modalTitle.getText());
		}
	}

	public void logoutTest(WebDriver driver) throws InterruptedException {
		Thread.sleep(2000);
		WebElement logout = driver.findElement(By
				.xpath("/html/body/div[1]/div/div[3]/span/a"));
		logout.click();
		Thread.sleep(2000);
		WebElement login = driver.findElement(By
				.xpath("/html/body/div[2]/div/div/h2"));
		assertEquals("Login", login.getText());
	}

	public void deleteButtonTest(WebDriver driver) throws InterruptedException {
		Thread.sleep(2000);
		WebElement selectBox = driver.findElement(By
				.xpath("//*[@id=\"table\"]/tbody/tr[1]/td[1]/input"));
		selectBox.click();
		WebElement deleteButton = driver.findElement(By
				.xpath("//*[@id=\"deleteMails\"]"));
		String style = deleteButton.getAttribute("style");
		style.trim();
		String[] styles = style.split(";");
		System.out.println(styles[1]);
		assertEquals(true, styles[1].equals(" display: block"));
	}

	public void selectRowsTest(WebDriver driver) throws InterruptedException {
		Thread.sleep(2000);
		Select select = new Select(driver.findElement(By
				.xpath("//*[@id=\"mark\"]")));
		select.selectByVisibleText("All");
		List<WebElement> selectBoxes = driver.findElements(By
				.cssSelector("input:checked[type='checkbox']"));
		System.out.println(selectBoxes.size());
		assertEquals(6, selectBoxes.size());
		select.selectByVisibleText("Unread");
		selectBoxes = driver.findElements(By
				.cssSelector("input:checked[type='checkbox']"));
		System.out.println(selectBoxes.size());
		assertEquals(4, selectBoxes.size());
		select.selectByVisibleText("Read");
		selectBoxes = driver.findElements(By
				.cssSelector("input:checked[type='checkbox']"));
		System.out.println(selectBoxes.size());
		assertEquals(2, selectBoxes.size());
		select.selectByVisibleText("None");
		selectBoxes = driver.findElements(By
				.cssSelector("input:checked[type='checkbox']"));
		System.out.println(selectBoxes.size());
		assertEquals(0, selectBoxes.size());
	}

	public void deleteTest(WebDriver driver) throws InterruptedException {
		Thread.sleep(2000);
		WebElement selectBox = driver.findElement(By
				.xpath("//*[@id=\"table\"]/tbody/tr[1]/td[1]/input"));
		selectBox.click();
		Thread.sleep(1000);
		WebElement deleteButton = driver.findElement(By
				.xpath("//*[@id=\"deleteMails\"]"));
		deleteButton.click();
		Thread.sleep(5000);
		List<WebElement> rows = driver.findElements(By.tagName("tr"));
		revertDelete();
		assertEquals(5, rows.size());
	}

	private void revertDelete() {
		String to = "webclienttest042015@gmail.com";
		String from = "incredible.kumda@gmail.com";
		String host = "smtp.gmail.com";
		Properties properties = System.getProperties();
		properties.setProperty("mail.smtp.host", host);
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.port", "25");
		Session session = Session.getInstance(properties,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(
								"incredible.kumda@gmail.com", "MADHURIMULGUND");
					}
				});

		try {
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(
					to));
			message.setSubject("This is the Subject Line!");
			message.setText("This is actual message");
			Transport.send(message);
			System.out.println("Sent message successfully....");
		} catch (MessagingException mex) {
			mex.printStackTrace();
		}

	}
	
	public void composeModalTest(WebDriver driver) throws InterruptedException{
		Thread.sleep(2000);
		WebElement composeButton = driver.findElement(By
				.xpath("//*[@id=\"compose\"]"));
		composeButton.click();
		Thread.sleep(2000);
		composeModalCloseButton(driver);
		composeButton.click();
		Thread.sleep(2000);
		sendButtonTest1(driver);
		Thread.sleep(2000);
		composeButton.click();
		Thread.sleep(2000);
		sendButtonTest2(driver);
		
	}
	
	private void composeModalCloseButton(WebDriver driver) throws InterruptedException{
		Set windows = driver.getWindowHandles();
		Iterator iterator = windows.iterator();
		while (iterator.hasNext()) {
			String popupHandle = iterator.next().toString();
			driver.switchTo().window(popupHandle);
			WebElement closeButton = driver.findElement(By
					.xpath("//*[@id=\"discard_draft\"]"));
			closeButton.click();
			Thread.sleep(1000);
			WebElement userName = driver.findElement(By.xpath("//*[@id=\"username\"]"));
			assertEquals("webclienttest042015@gmail.com",userName.getText());
		}
	}
	
	private void sendButtonTest1(WebDriver driver){
		Set windows = driver.getWindowHandles();
		Iterator iterator = windows.iterator();
		while (iterator.hasNext()) {
			String popupHandle = iterator.next().toString();
			driver.switchTo().window(popupHandle);
			WebElement sendButton = driver.findElement(By.xpath("//*[@id=\"send_mail\"]"));
			sendButton.click();
			this.getWait().until(ExpectedConditions.alertIsPresent());
			Alert alert = driver.switchTo().alert();
			assertEquals("To: field left blank.", alert.getText());
			alert.accept();
		}
		
	}
	
	private void sendButtonTest2(WebDriver driver) throws InterruptedException{
		Set windows = driver.getWindowHandles();
		Iterator iterator = windows.iterator();
		while (iterator.hasNext()) {
			String popupHandle = iterator.next().toString();
			driver.switchTo().window(popupHandle);
			WebElement toAddress = driver.findElement(By.xpath("//*[@id=\"to_email_ids\"]"));
			toAddress.clear();
			toAddress.sendKeys("webclienttest2015@gmail.com");
			WebElement subject = driver.findElement(By.xpath("//*[@id=\"subject\"]"));
			subject.clear();
			subject.sendKeys("send message functional test");
			WebElement textArea = driver.findElement(By.xpath("//*[@id=\"send_mail_form\"]/div[2]/div[3]"));
			textArea.clear();
			textArea.sendKeys("Hi... Conducting a functional test");
			WebElement sendButton = driver.findElement(By.xpath("//*[@id=\"send_mail\"]"));
			sendButton.click();
			Thread.sleep(5000);
			Alert alert = driver.switchTo().alert();
			assertEquals("Mesage sent successfully.", alert.getText());
			alert.accept();
		}
	}
	
	@Test
	public void homePageTest() throws InterruptedException{
		this.getWebBrowser();
		WebDriver driver = this.getDriver();
		this.login("webclienttest042015@gmail.com", "webclient042015");
		this.getWait().until(
				ExpectedConditions.presenceOfElementLocated(By
						.xpath("//*[@id=\"username\"]")));
		Thread.sleep(2000);
		composeButtonTest(driver);
		composeModalTest(driver);
		selectRowsTest(driver);
		deleteButtonTest(driver);
		deleteTest(driver);
		logoutTest(driver);
		this.quitWebBrowser();
	}
}
