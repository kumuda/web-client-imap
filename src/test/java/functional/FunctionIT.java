package functional;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.mail.FetchProfile;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;

import model.Mail;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import util.SpringMongoConfig;

public class FunctionIT {
	
	private WebDriver driver;
	private Wait<WebDriver> wait;
	private static Logger log = Logger.getLogger(FunctionIT.class);
	public WebDriver getDriver() {
		return driver;
	}

	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}

	public Wait<WebDriver> getWait() {
		return wait;
	}

	public void setWait(WebDriverWait wait) {
		this.wait = wait;
	}

	protected void getWebBrowser() {
		driver = new FirefoxDriver();
		driver.get("http://localhost:8080/Webclient/");
		wait = new FluentWait<WebDriver>( driver )
                .withTimeout(30, TimeUnit.SECONDS)
                .pollingEvery(5, TimeUnit.SECONDS)
                .ignoring( NoSuchElementException.class ) 
                .ignoring( StaleElementReferenceException.class ) ;
 
	}

	protected void quitWebBrowser() {
		driver.quit();
	}

	protected void login(String username, String password) {
		WebElement usernameField = driver.findElement(By
				.xpath("//*[@id=\"username\"]"));
		usernameField.sendKeys(username);
		WebElement passwordField = driver.findElement(By
				.xpath("//*[@id=\"loginPassword\"]"));
		passwordField.sendKeys(password);
		WebElement submit = driver.findElement(By
				.xpath("/html/body/div[2]/div/div/form/fieldset[3]/button"));
		submit.submit();
	}

	protected void logout() {
		WebElement logout = driver.findElement(By
				.xpath("/html/body/div[1]/div[2]/ul/div/div/span/a"));
		logout.click();
	}
	
	
	
}
