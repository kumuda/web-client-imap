package service;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.imageio.stream.FileImageInputStream;
import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.URLName;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import model.Attachment;
import model.User;

import org.apache.commons.io.IOUtils;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.icegreen.greenmail.user.GreenMailUser;
import com.icegreen.greenmail.user.UserException;
import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.GreenMailUtil;
import com.icegreen.greenmail.util.ServerSetup;
import com.icegreen.greenmail.util.ServerSetupTest;

public class ImapTest {

	private static final String USER1_PASSWORD = "abcdef123";
	private static final String USER1_NAME = "hascode";
	private static final String EMAIL_USER1_ADDRESS = "hascode@localhost.com";
	private static final String EMAIL_USER2_ADDRESS = "someone@localhost.com";
	private static final String USER2_PASSWORD = "someone";
	private static final String USER2_NAME = "someone";
	private static final String EMAIL_SUBJECT = "Test E-Mail";
	private static final String EMAIL_TEXT = "This is a test e-mail.";
	private static final String LOCALHOST = "127.0.0.1";
	private GreenMail mailServer;

	ImapService imapService;

	@Before
	public void setUp() throws MessagingException, UserException {
		ServerSetup[] testSetup = {ServerSetupTest.IMAP,ServerSetupTest.SMTP};
		mailServer = new GreenMail(testSetup);
		imapService = (ImapServiceImpl) ImapServiceImpl.getInstance();
		mailServer.start();
		Store store = createStore();
		imapService.getStoreMap().put(EMAIL_USER1_ADDRESS, store);
	}

	@After
	public void tearDown() {
		mailServer.stop();
		imapService = (ImapServiceImpl) ImapServiceImpl.getInstance();
		imapService.getStoreMap().clear();

	}

	private Store createStore() throws MessagingException {
		GreenMailUser user = mailServer.setUser(EMAIL_USER1_ADDRESS,
				USER1_NAME, USER1_PASSWORD);
		Properties props = new Properties();
		Session session = Session.getInstance(props);
		URLName urlName = new URLName("imap", LOCALHOST,
				ServerSetupTest.IMAP.getPort(), null, user.getLogin(),
				user.getPassword());
		Store store = session.getStore(urlName);
		store.connect();
		return store;
	}

	@Test
	public void getMessageCountTest1() throws MessagingException, UserException {
		this.setMessage();
		imapService = (ImapServiceImpl) ImapServiceImpl.getInstance();
		User user = new User();
		user.setUserName(EMAIL_USER1_ADDRESS);
		user.setPassword(USER1_PASSWORD);
		int messageCount = imapService.getMessageCount("Inbox", user);
		assertNotEquals(0, messageCount);
		assertEquals(1, messageCount);
	}

	@Test
	public void deleteMails() throws MessagingException, UserException{
		this.setMessage();
		int messageCount = this.getMessageCount();
		assertEquals(1, messageCount);
		imapService = (ImapServiceImpl) ImapServiceImpl.getInstance();
		User user = new User();
		user.setUserName(EMAIL_USER1_ADDRESS);
		user.setPassword(USER1_PASSWORD);
		List<Integer> messageNumbers = new ArrayList<Integer>();
		messageNumbers.add(1);
		imapService.deleteMails("Inbox", messageNumbers, user);
		messageCount = this.getMessageCount();
		assertEquals(0, messageCount);
	}
	
	private void setMessage() throws AddressException, MessagingException,
			UserException {
		GreenMailUser user = mailServer.setUser(EMAIL_USER1_ADDRESS,
				USER1_NAME, USER1_PASSWORD);
		MimeMessage message = new MimeMessage(this.sessionInjection());
		message.setFrom(new InternetAddress(EMAIL_USER2_ADDRESS));
		message.addRecipient(Message.RecipientType.TO, new InternetAddress(
				EMAIL_USER1_ADDRESS));
		message.setSubject(EMAIL_SUBJECT);
		message.setText(EMAIL_TEXT);
		user.deliver(message);
		
	}
	
/*	@Test
	public void sendMessageTest() throws IOException, MessagingException, UserException{
		this.setMessage();
		this.sessionInjection();
		JSONObject jsonObject = new JSONObject();
		InternetAddress[] addresses = new InternetAddress[1];
		addresses[0] = new InternetAddress(EMAIL_USER1_ADDRESS,"USER1_NAME");
		jsonObject.put("subject", "Testing send mail api");
		jsonObject.put("addresses", addresses);
		jsonObject.put("snippet", "Hi,Checking the send mail api with attachment");
		User user = new User();
		user.setUserName(EMAIL_USER2_ADDRESS);
		user.setPassword(USER2_PASSWORD);
		imapService = (ImapServiceImpl)ImapServiceImpl.getInstance();
		imapService.sendMail(jsonObject,user);
		GreenMailUser greenMailuser = mailServer.setUser(EMAIL_USER1_ADDRESS,
				USER1_NAME, USER1_PASSWORD);
		Properties props = new Properties();
		Session session = Session.getInstance(props);
		URLName urlName = new URLName("imap", LOCALHOST,
				ServerSetupTest.IMAP.getPort(), null, greenMailuser.getLogin(),
				greenMailuser.getPassword());
		Store store = session.getStore(urlName);
		store.connect();
		Folder folder = store.getFolder("Inbox");
		folder.open(Folder.READ_ONLY);
		int messageCount = folder.getMessageCount();
		assertEquals(2,messageCount);
	}*/
	
	private int getMessageCount() throws MessagingException{
		
		Store store = createStore();
		Folder folder = store.getFolder("INBOX");
		folder.open(Folder.READ_ONLY);
		int messageCount = folder.getMessageCount();
		folder.close(false);
		
		return messageCount;
	}
	
	
	private Session sessionInjection(){
        Properties props = System.getProperties();
        props.put("mail.smtp.host", LOCALHOST);
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "25");
        Session session = Session.getInstance(props, null);
        ImapServiceImpl.getSessionMap().put(EMAIL_USER1_ADDRESS, session);
        
        return session;
	}
	
	@Test
	public void getMessageBody() throws Exception{
	this.setMessage();
	System.out.println("Test msg count" + this.getMessageCount());
	User user = new User();
	user.setUserName(EMAIL_USER1_ADDRESS);
	user.setPassword(USER1_PASSWORD);
	imapService = (ImapServiceImpl) ImapServiceImpl.getInstance();
	HashMap<String,Object> messageBody = imapService.getMessageBody("Inbox", 1, "false",user);
	String messageText = (String)messageBody.get("messageBody");
	ArrayList<Attachment> attachments = (ArrayList<Attachment>)messageBody.get("attachments");
	assertEquals(EMAIL_TEXT, messageText);
	//assertEquals("spurti cv 2015.pdf", attachments.get(0).getFileName());
	}
	
	private void setMessageWithAttachment() throws AddressException, MessagingException, UserException, IOException{
		
		GreenMailUtil.sendAttachmentEmail(EMAIL_USER1_ADDRESS, EMAIL_USER2_ADDRESS, EMAIL_SUBJECT, EMAIL_TEXT, new byte[]{0,1,2},"image/gif", "spurti cv 2015.pdf" , "/",ServerSetup.SMTP);

	}

}
