package service;

import java.net.UnknownHostException;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import com.mongodb.MongoException;

public class MongoDbUtil {
    
    private static DB database;
    
    static{
        try {
            MongoClient mongo=new MongoClient("localhost",27017);
            database=mongo.getDB("webclient");
        } catch (MongoException ex) {
        	throw new IllegalArgumentException(ex);
        } catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public static DBCollection getCollection(String collectionName){
        return database.getCollection(collectionName);
    }
}